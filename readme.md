### Installation


With a new virtual env activated
```bash
pip install -r requirements.txt
```

### Customize django settings
To customize some django settings, copy to the root project folder the 
file "settings.ini.example" and rename it to "settings.ini".

The available parameters are:

```ini
[settings]

# Security
DEBUG=True
SECRET_KEY=4cn68iga94@**2x9vb1f*-104pe%%*-u-%%#%%1wh!r(+mjiza@y$

# Database
DATABASE_ENGINE=django.db.backends.postgresql_psycopg2
DATABASE_HOST=localhost
DATABASE_NAME=weatherdb
DATABASE_USER=root
DATABASE_PASSWORD=1234qwer
```
