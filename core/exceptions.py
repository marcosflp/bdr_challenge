class GoogleGeocodingNotFountException(Exception):
    pass


class ManyConsecutiveAttemptsException(Exception):
    pass
