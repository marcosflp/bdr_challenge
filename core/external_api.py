import abc
import json
import time

import requests

from core import exceptions


class BaseApi(metaclass=abc.ABCMeta):
    """"
    Base API
    """
    BASE_URL = ''
    API_KEY = ''
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }

    def get(self, endpoint, params=None):
        if params is None:
            params = dict()
        elif not isinstance(params, dict):
            raise TypeError('the argument "params" must be a dict type')
        return self._conn('GET', endpoint, params=params)

    def _conn(self, method, endpoint, data=None, params=None):
        """
        Returns: HttpResponse
        Raises:
            requests.exceptions.HTTPError
            ManyConsecutiveAttemptsException
        """
        url = self.create_url(endpoint)

        current_delay = 0.1  # Set the initial retry delay to 100ms.
        max_delay = 3  # Set max delay to 3s.

        while True:
            try:
                response = requests.request(method, url, json=data, params=params)
                break
            except IOError:
                pass
            except requests.exceptions.ConnectTimeout:
                pass

            if current_delay > max_delay:
                raise exceptions.ManyConsecutiveAttemptsException

            time.sleep(current_delay)
            current_delay *= 2

        if not response.ok:
            raise requests.exceptions.HTTPError('Bad response')

        return response

    @abc.abstractmethod
    def create_url(self, endpoint):
        """
        Abstract method that must be implemented in subclass.
        Since each api has different urls it's necessary to implement the url creation for each subclass.
        """
        pass


class GoogleGeocodingApi(BaseApi):
    BASE_URL = 'https://maps.googleapis.com/maps/api'
    API_KEY = 'AIzaSyAg6rz9WIBVRKGEo-Zqx9tjDxSTF4Yk6rs'
    FORMAT_RESPONSE = 'json'  # or xml

    def __init__(self):
        self.data = None

    def reverse_address(self, address):
        """
        Reverse address is the process of finding the latitude and longitude of an address.

        Returns (dict): {
            'name': 'Florianópolis-SC',
            'latitude': ,
            'longitude': }
        """
        response = self.get('geocode', params={'address': address})

        self.data = json.loads(response.text)
        if len(self.data['results']) == 0:
            raise exceptions.GoogleGeocodingNotFountException

        location = self.data['results'][0]['geometry']['location']

        # Set decimal places to max 6
        data = {
            'name': '{}-{}, {}'.format(self._get_city(), self._get_state(), self._get_country()),
            'latitude': '{:.6f}'.format(location['lat']),
            'longitude': '{:.6f}'.format(location['lng'])
        }
        return data

    def _get_country(self):
        address_component_list = self.data['results'][0].get('address_components', [])
        for address_component in address_component_list:
            if 'country' in address_component['types']:
                return address_component['short_name']
        else:
            return None

    def _get_state(self):
        address_component_list = self.data['results'][0].get('address_components', [])
        for address_component in address_component_list:
            if 'administrative_area_level_1' in address_component['types']:
                return address_component['short_name']
        else:
            return None

    def _get_city(self):
        address_component_list = self.data['results'][0].get('address_components', [])
        for address_component in address_component_list:
            if 'administrative_area_level_3' in address_component['types']:
                return address_component['long_name']
            elif 'administrative_area_level_2' in address_component['types']:
                return address_component['long_name']
        else:
            return None

    def create_url(self, endpoint):
        return '{}/{}/{}'.format(self.BASE_URL, 'geocode', self.FORMAT_RESPONSE)

    def _conn(self, method, endpoint, data=None, params=None):
        # GoogleApi uses the Query Params to receive the api key.
        # It's necessary override this method to add the key on the request Params
        params['key'] = self.API_KEY
        return super()._conn(method, endpoint, data, params)


class DarkSkyApi(BaseApi):
    BASE_URL = 'https://api.darksky.net/forecast'
    API_KEY = '96b47f43f97fdc8f03d64785b9816c5f'

    def get_weather_temperature_from(self, latitude, longitude):
        """
        Get the temperature from the given latitude and longitude.
        Returns (float): temperature in Celsius
        """
        endpoint = '{},{}'.format(latitude, longitude)
        params = {
            'lang': 'pt',
            'units': 'auto',
            'exclude': 'daily,hourly,minutely,alerts,flags,'
        }
        response = self.get(endpoint, params=params)
        if not response.ok:
            raise requests.exceptions.HTTPError('Bad response')

        data = json.loads(response.text)

        return data['currently']['temperature']

    def create_url(self, endpoint, params=None):
        return '{}/{}/{}'.format(self.BASE_URL, self.API_KEY, endpoint)


GoogleGeocodingApi = GoogleGeocodingApi()
DarkSkyApi = DarkSkyApi()
