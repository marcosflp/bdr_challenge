import datetime

from django.db import models
from django.utils import timezone


class Weather(models.Model):
    name = models.CharField(max_length=255, blank=True, default='')
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)

    temperature = models.DecimalField('Temperature in °C', max_digits=6, decimal_places=3)
    temperature_last_update = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ('latitude', 'longitude')

    def __str__(self):
        return 'Temperature at "{}" is {}°C'.format(self.name, self.temperature)

    def update_temperature(self, current_temperature):
        if not self.is_outdated_temperature:
            return None

        self.temperature_last_update = timezone.now()
        self.temperature = current_temperature
        self.save()

    @property
    def is_outdated_temperature(self):
        next_hour = timezone.now() + datetime.timedelta(hours=1)
        if self.temperature_last_update >= next_hour:
            return True
        return False

    @property
    def temperature_in_f(self):
        """
        Returns the temperature in Fahrenheit.
        """
        return self.temperature*(9/5) + 32
