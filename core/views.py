from django.views.generic import FormView

from core.external_api import GoogleGeocodingApi, DarkSkyApi
from core.forms import WeatherLocationForm
from core.models import Weather


class HomeView(FormView):
    http_method_names = ['get']
    template_name = 'core/home.html'
    form_class = WeatherLocationForm

    def get_initial(self):
        initial = super(HomeView, self).get_initial()
        initial['location'] = self.request.GET.get('location', '')
        return initial

    def get_context_data(self, **kwargs):
        data = super(HomeView, self).get_context_data(**kwargs)

        location_search = self.request.GET.get('location', None)
        if location_search:
            data['weather'] = self.get_weather(location_search)

        return data

    def get_weather(self, location):
        address_location = GoogleGeocodingApi.reverse_address(address=location)

        try:
            weather_obj = Weather.objects.get(
                latitude=address_location['latitude'],
                longitude=address_location['longitude'],
            )

            if weather_obj.is_outdated_temperature:
                current_temperature = self.get_current_temperature_from(
                    address_location['latitude'],
                    address_location['longitude'],
                )
                weather_obj.update_temperature(current_temperature)

        except Weather.DoesNotExist:
            current_temperature = self.get_current_temperature_from(
                address_location['latitude'],
                address_location['longitude']
            )
            weather_obj = Weather.objects.create(
                latitude=address_location['latitude'],
                longitude=address_location['longitude'],
                name=address_location['name'],
                temperature=current_temperature
            )

        return weather_obj

    def get_current_temperature_from(self, lat, lng):
        """
        Returns (float): temperature in Celsius
        """
        temperature = DarkSkyApi.get_weather_temperature_from(
            latitude=lat,
            longitude=lng
        )
        return temperature
