from django import forms

from core.models import Weather


class WeatherLocationForm(forms.Form):
    location = forms.CharField(max_length=255)


class WeatherForm(forms.ModelForm):

    class Meta:
        model = Weather
        fields = ('name', 'latitude', 'longitude', 'temperature', 'temperature_last_update')
